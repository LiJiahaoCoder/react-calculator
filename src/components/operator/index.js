import React, { Component } from 'react';
// style
import './index.less';

export default class Operator extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { operator } = this.props;
    this.props.handleInputChange('operator', operator);
  }

  render() {
    return (
      <button onClick={this.handleClick} className='operator-button'>{this.props.operator}</button>
    );
  }
}