import React, { Component } from 'react';
// style
import './index.less';

export default class Equal extends Component {
  constructor(props) {
    super(props);

    this.handleClickEqual = this.handleClickEqual.bind(this);
  }

  handleClickEqual() {
    let { number_1, number_2, result, operator } = this.props.data;
    switch (operator) {
      case '+':
        result = number_1 + number_2;
        break;
      case 'x':
        result = number_1 * number_2;
        break;
      case '-':
        result = number_1 - number_2;
        break;
      case '/':
        result = number_1 / number_2;
        break;
      default:
        break;
    }
    this.props.handleClickEqual('result', result);
  }
  
  render() {
    return (
      <button
        onClick={this.handleClickEqual}
        className='equal-button'
      >
        =
      </button>
    );
  }
}