import React, { Component } from 'react';
// style
import './index.less';

export default class InputItem extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { value } = e.currentTarget;
    const { numberName } = this.props;
    this.props.handleInputChange(numberName, value)
  }

  render() {
    const { number } = this.props;
    return (
      <input
        className='input-item'
        type='text'
        value={number}
        onChange={this.handleChange}
      />
    );
  }
}
