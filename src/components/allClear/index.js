import React, { Component } from 'react';
// style
import './index.less';

export default class AllClear extends Component {
  constructor(props) {
    super(props);
    this.handleClickAllClear = this.handleClickAllClear.bind(this);
  }

  handleClickAllClear() {
    this.props.handleClickAllClear('clear', 'all');
  }
  
  render() {
    return (
      <button
        onClick={this.handleClickAllClear}
        className='all-clear-button'
      >
        AC
      </button>
    );
  }
}