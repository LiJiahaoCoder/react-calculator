import React, { Component } from 'react';
// style
import './index.less';
//containers
import OperatorContainer from '../../containers/operator';
import InputContainer from '../../containers/input/index';
import CountContainer from '../../containers/count';
// components
import Screen from '../screen';

export default class Calculator extends Component {
  constructor(props) {
    super(props);

    this.state = {
      number_1: 0,
      number_2: 0,
      result: 0,
      operator: ''
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(key, value) {
    if (key === 'clear') {
      this.setState({
        number_1: 0,
        number_2: 0,
        result: 0,
        operator: ''
      });
    } else {
      this.setState({[key]: value});
    }
  }

  render() {
    const { number_1, number_2, result, operator } = this.state;
    return (
      <div className='cal-body'>
        <Screen result={result} />
        <div className='input-container'>
          <OperatorContainer
            handleInputChange={this.handleChange}
          />
          <InputContainer
            handleInputChange={this.handleChange}
            number={[number_1, number_2]}
          />
        </div>
        <CountContainer
          handleInputChange={this.handleChange}
          data={{number_1, number_2, result, operator}}
        />
      </div>
    );
  }
}
