import React, { Component } from 'react';
// style
import './index.less';

export default class Screen extends Component {
  render() {
    const { result } = this.props;
    return (
      <section>
        <input className='screen' disabled value={result} />
      </section>
    );
  }
}
