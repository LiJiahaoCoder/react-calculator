import React from 'react';
// style
import './index.less';
// components
import Operator from '../../components/operator';

export default function OperatorContainer(props) {
  const OPERATOR = ['+', 'x', '-', '/'];
  return (
    <section className='oprator-container'>
      {
        OPERATOR.map(operator => <Operator
          handleInputChange={props.handleInputChange}
          key={operator}
          operator={operator}
        />)
      }
    </section>
  );
}