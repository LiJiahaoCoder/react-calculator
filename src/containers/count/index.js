import React from 'react';
// style
import './index.less';
// components
import AllClear from '../../components/allClear';
import Equal from '../../components/equal';

export default function CountContainer(props) {
  const { data } = props;
  return (
    <section className='count-container'>
      <AllClear handleClickAllClear={props.handleInputChange} />
      <Equal data={data} handleClickEqual={props.handleInputChange} />
    </section>
  );
}
