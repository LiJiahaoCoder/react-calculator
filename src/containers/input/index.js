import React from 'react';
// style
import './index.less';
// components
import InputItem from '../../components/input/index';

export default function InputContainer(props) {
  const [number_1, number_2] = props.number;
  return (
    <section className='inputs'>
      <InputItem handleInputChange={props.handleInputChange} numberName='number_1' number={number_1} />
      <InputItem handleInputChange={props.handleInputChange} numberName='number_2' number={number_2} />
    </section>
  );
}
